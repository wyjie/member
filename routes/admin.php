<?php

Auth::routes();

    Route::get('/', 'UserController@index');
    Route::group(['prefix' => '/user', 'as' => 'user::'], function () {
        Route::get('/', 'UserController@index');
        Route::post('/', 'UserController@create');
        Route::get('/create', 'UserController@show');
        Route::get('/skills', 'UserController@skillsShow');
        Route::get('/{id}', 'UserController@edit');
        Route::post('/update', 'UserController@update');
        Route::post('/skills', 'UserController@skills');
        Route::get('/{id}/{status}', 'UserController@status');
    });

    Route::group(['prefix' => '/portfolio', 'as' => 'portfolio::'], function () {
        Route::get('/', 'PortfolioController@index');
        Route::post('/', 'PortfolioController@create');
        Route::get('/create', 'PortfolioController@show');
        Route::get('/{id}', 'PortfolioController@edit');
        Route::post('/update', 'PortfolioController@update');
        Route::get('/{id}/{status}', 'PortfolioController@status');
    });

    Route::group(['prefix' => '/image', 'as' => 'image::'], function () {
        Route::post('/logo', 'ImageController@putLogo');
        Route::post('/image', 'ImageController@putImage');
        Route::post('/portrait', 'ImageController@putPortrait');
    });
