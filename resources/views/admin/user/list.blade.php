@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-head-line">User Management</h1>
                </div>
            </div>
            <!-- /. ROW  -->

            <div class="row" id="pages" data-pages="{{ $pages }}">
                <div class="col-md-12">
                    <!--   Kitchen Sink -->
                    <div class="panel panel-default">
                        <div class="panel-heading" style="height: 46px;">
                            User List
                            <a href="{{ url('/admin/user/skills') }}"><button style="float: right; margin-top: -4px;" type="button" class="btn btn-primary">Skills</button></a>
                                <a href="{{ url('/admin/user/create') }}"><button style="float: right; margin-top: -4px;" type="button" class="btn btn-primary">Add</button></a>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Full Name</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            <th>Exp</th>
                                            <th>Gender</th>
                                            <th>Created At</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $item)
                                            <tr>
                                                <th>{{ $item->id }}</th>
                                                <th>{{ $item->full_name }}</th>
                                                <th>{{ $item->email }}</th>
                                                <th>{{ $item->mobile }}</th>
                                                <th>{{ $item->experience }}</th>
                                                <th>@if($item->gender == 1) Male @else Female @endif</th>
                                                <th>{{ $item->created_at }}</th>
                                                <th id="status{{$item->id}}">@if($item->deleted == 0) Enable @else Disable @endif</th>
                                                <th>
                                                    <button @if($item->deleted == 1) style="display: none" @endif data-id="{{ $item->id }}" data-status="1" type="button"  class="btn btn-danger delete">Disable</button>
                                                    <button @if($item->deleted == 0) style="display: none" @endif data-id="{{ $item->id }}" data-status="0" type="button"  class="btn btn-danger ref">Enable</button>

                                                    <a href="/admin/user/{{ $item->id }}">
                                                        <button id="edit" type="button" class="btn btn-primary">Edit</button>
                                                    </a>

                                                    <a href="/user/{{ $item->key }}" target="_blank">
                                                        <button id="view" type="button" class="btn btn-primary">View</button>
                                                    </a>
                                                </th>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="page"></div>
                    </div>
                    <!-- End  Kitchen Sink -->
                </div>
            </div>

        </div>
        <!-- /. PAGE INNER  -->
    </div>
@endsection

@section('after_script')
    <script>
        $(function () {
            var edit_button = $("#edit");
            var ref_button = $(".ref");
            var del_button = $(".delete");
            var pages = $("#pages").data("pages");


            layui.use('laypage', function(){
                var laypage = layui.laypage;

                //分页
                laypage.render({
                    elem: 'page',
                    count: pages,
                    prev: 'prev',
                    next: "next",
                    curr: function(){
                        var page = location.search.match(/page=(\d+)/);
                        return page ? page[1] : 1;
                    }(),
                    jump: function(e, first){ //触发分页后的回调
                        if(!first){ //一定要加此判断，否则初始时会无限刷新
                            location.href = '?page='+e.curr;
                        }
                    },
                });
            });

            //删除，恢复
            del_button.click(function () {
                var user_id = $(this).data('id');
                var deleted = $(this).data('status');
                $.ajax({
                    type: "GET",
                    url: "{{ url('/admin/user') }}" + '/' + user_id + '/' + deleted,
                    success: function(data){
                        if (data.code == 1) {
                            layui.use('layer', function(){
                                var layer = layui.layer;

                                layer.msg('success', {time: 2000, icon:6});
                            });
                            del_button.hide();
                            ref_button.show();
                            $("#status"+user_id).html("Disable");
                        }
                    }
                });
            });

            //恢复
            ref_button.click(function () {
                var user_id = $(this).data('id');
                var deleted = $(this).data('status');
                $.ajax({
                    type: "GET",
                    url: "{{ url('/admin/user') }}" + '/' + user_id + '/' + deleted,
                    success: function(data){
                        if (data.code == 1) {
                            layui.use('layer', function(){
                                var layer = layui.layer;

                                layer.msg('success', {time: 2000, icon:6});
                            });
                            ref_button.hide();
                            del_button.show();
                            $("#status"+user_id).html("Enable");
                        } else {
                            layui.use('layer', function(){
                                var layer = layui.layer;

                                layer.msg('error', {time: 2000, icon:2});
                            });
                        }
                    }
                });
            });
        })
    </script>
@endsection