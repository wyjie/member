@extends('layouts.admin')
@section('before_style')
    <style>
        .layui-input, .layui-textarea {
            display: block;
            width: 95%;
            padding-left: 10px;
        }
        .layui-form-label {
            float: left;
            display: block;
            padding: 9px 15px;
            width: 123px;
            font-weight: 400;
            line-height: 20px;
            text-align: right;
        }
    </style>
@endsection
@section('content')
    <div id="page-wrapper">
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="page-head-line">Edit User</h2>
                </div>
            </div>
                        <!-- /. ROW  -->
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                Edit User
                            </div>
                            <div class="panel-body">
                                <form class="layui-form" method="POST" action="{{ url('admin/user/skills') }}">


                                    <div class="layui-form-item">
                                        <label class="layui-form-label">User</label>
                                        <div class="layui-input-block">
                                            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                            <select name="id">
                                                @foreach($users as $item)
                                                    <option value="{{ $item->id }}" >{{ $item->full_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="layui-form-item">
                                        <label class="layui-form-label">Skills</label>
                                        <div class="layui-input-block">
                                            <input type="text" name="skills" value="" autocomplete="off" class="layui-input">
                                        </div>
                                    </div>

                                    <button class="layui-btn" lay-submit >Update</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

        </div>
        <!-- /. PAGE INNER  -->
    </div>
@endsection

@section('after_script')
    <link href="{{ asset('css/jquery.tagsinput.css') }}" rel="stylesheet" />
    <script src="{{ asset('js/jquery.tagsinput.min.js') }}"></script>
    <script>
        $(function () {
            var addSkill = $("#add_skill");
            var skillTBody = $("#skills_tbody");
            layui.use('form', function(){
                var form = layui.form;
            });


            layui.use('rate', function(){
                var rate = layui.rate;

                //渲染
                var ins1 = rate.render({
                    elem: '#test1'  //绑定元素
                });
            });


        layui.use('laydate', function(){
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                    elem: '.edu_start_at' //指定元素
                    ,lang: 'en'
                });
            });

            layui.use('laydate', function(){
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                    elem: '.edu_end_at' //指定元素
                    ,lang: 'en'
                });
            });

            layui.use('upload', function(){
                var upload = layui.upload;

                //upload logo
                upload.render({
                    elem: '#portrait_but' //绑定元素
                    ,url: '/admin/image/portrait' //上传接口
                    ,headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                    ,done: function(res){
                        $("#portrait_img").attr('src', res.url);
                        $("#portrait").val(res.url);
                    }
                    ,error: function(){
                        //请求异常回调
                    }
                });
            });


            //删除指定的下标
            Array.prototype.removeByTag = function(Tag) {
                for(var i=0; i<this.length; i++) {
                    if(this[i] == Tag) {
                        this.splice(i, 1);
                        break;
                    }
                }
            };

            //过滤特殊字符
            function stripscript(s) {
                var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]")
                var rs = "";
                for (var i = 0; i < s.length; i++) {
                    rs = rs+s.substr(i, 1).replace(pattern, '');
                }
                return rs;
            }

            //添加标签
            function onAddTagInSkills(tag) {
                //非法字符过滤
                tag = stripscript(tag);
                var post_skills = $(this).prev();
                var post_skills_val = post_skills.val();
                post_skills.val(post_skills_val + ',' + tag);
            }

            //删除标签
            function onRemoveTagInSkills(tag) {
                //非法字符过滤
                tag = stripscript(tag);
                var post_skills = $(this).prev();
                var post_skills_val = post_skills.val();
                var post_skills_arr = post_skills_val.split(",");// 在每个逗号(,)处进行分解.
                post_skills_arr.removeByTag(tag);
                post_skills.val(post_skills_arr.join(','));
            }

            //修改标签 -- 暂不支持
            function onChangeTagInSkills(input,tag) {}

            var labels = $(".labels");
            labels.tagsInput({
                'interactive':true, //是否允许添加标签，false为阻止
                'defaultText':'', //默认文字
                'onAddTag':onAddTagInSkills, //增加标签的回调函数
                'onRemoveTag':onRemoveTagInSkills, //删除标签的回调函数
                'onChange' : onChangeTagInSkills, //改变一个标签时的回调函数
                'removeWithBackspace' : true, //是否允许使用退格键删除前面的标签，false为阻止
                'minChars' : 1, //每个标签的小最字符
                'maxChars' : 20, //每个标签的最大字符，如果不设置或者为0，就是无限大
                'placeholderColor' : '#FF5722' //设置defaultText的颜色
            });

            var tips = $(".tips");
            tips.tagsInput({
                'interactive':true, //是否允许添加标签，false为阻止
                'defaultText':'', //默认文字
                'onAddTag':onAddTagInSkills, //增加标签的回调函数
                'onRemoveTag':onRemoveTagInSkills, //删除标签的回调函数
                'onChange' : onChangeTagInSkills, //改变一个标签时的回调函数
                'removeWithBackspace' : true, //是否允许使用退格键删除前面的标签，false为阻止
                'minChars' : 1, //每个标签的小最字符
                //'maxChars' : 20, //每个标签的最大字符，如果不设置或者为0，就是无限大
                'placeholderColor' : '#FF5722' //设置defaultText的颜色
            });

            addSkill.click(function () {
                var i = Math.random();
                var _html = '<tr><td>'+
                        '<textarea name="skills['+i+'][name]" style="min-height: 18px !important;" class="layui-textarea"></textarea>'+
                        '</td><td>'+
                        '<textarea name="skills['+i+'][time]" style="min-height: 18px !important;" class="layui-textarea"></textarea></td><td></td></tr>';
                skillTBody.append(_html);
            });
        });
    </script>
@endsection