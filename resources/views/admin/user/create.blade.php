@extends('layouts.admin')
@section('before_style')
    <style>
        .layui-input, .layui-textarea {
            display: block;
            width: 95%;
            padding-left: 10px;
        }
        .layui-form-label {
            float: left;
            display: block;
            padding: 9px 15px;
            width: 123px;
            font-weight: 400;
            line-height: 20px;
            text-align: right;
        }
    </style>
@endsection
@section('content')
    <div id="page-wrapper">
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="page-head-line">Add User</h2>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <!-- /. ROW  -->
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            Add User
                        </div>
                        <div class="panel-body">
                            <form class="layui-form " method="POST" action="{{ url('admin/user') }}">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">First Name</label>
                                    <div class="layui-input-block">
                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                        <input type="text" name="first_name" placeholder="Please enter first name" autocomplete="off" class="layui-input">
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <label class="layui-form-label">Last Name</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="last_name" placeholder="Please enter la name" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">Key</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="key" placeholder="Please enter key" autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">Email</label>
                                    <div class="layui-input-block">
                                        <input type="email" name="email" placeholder="Please enter email" autocomplete="off" class="layui-input">
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <label class="layui-form-label">Mobile</label>
                                    <div class="layui-input-block">
                                        <input type="number" name="mobile" placeholder="Please enter mobile" autocomplete="off" class="layui-input">
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <label class="layui-form-label">Member Type</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="member_type" lay-verify="required" placeholder="Please enter member type" autocomplete="off" class="layui-input">
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <label class="layui-form-label">Gender</label>
                                    <div class="layui-input-block">
                                        <select name="gender" lay-verify="required">
                                            <option value="1">Male</option>
                                            <option value="2">Female</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <label class="layui-form-label">Location</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="location" required  lay-verify="required" placeholder="Please enter location." autocomplete="off" class="layui-input">
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <label class="layui-form-label">Preferred Environment </label>
                                    <div class="layui-input-block">
                                        <input type="text" name="preferred_environment" required  lay-verify="required" placeholder="Please enter Preferred Environment ." autocomplete="off" class="layui-input">
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <label class="layui-form-label">Experience</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="experience" required  lay-verify="required" placeholder="Please enter Experience(x year)." autocomplete="off" class="layui-input">
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <label class="layui-form-label">Profile Verified</label>
                                    <div class="layui-input-block">
                                        <input type="checkbox" name="profile_verified" lay-skin="switch">
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <label class="layui-form-label">Portrait</label>
                                    <div class="layui-input-block">
                                        <input id="portrait" class="form-control" name="portrait"  type="hidden">
                                        <img style="max-height: 200px; max-width: 200px" class="image-url" id="portrait_img" src="{{ url('image/M.png') }}" alt="">
                                        <div>
                                            <button type="button" class="layui-btn image-but" id="portrait_but">
                                                <i class="layui-icon">&#xe67c;</i>upload image
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">Amazing</label>
                                    <div class="layui-input-block">
                                        <textarea id="amazing" name="amazing" placeholder="Please enter body..." class="layui-textarea"></textarea>
                                    </div>
                                </div>

                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">Description</label>
                                    <div class="layui-input-block">
                                        <textarea id="description" name="description" placeholder="Please enter body..." class="layui-textarea"></textarea>
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <label class="layui-form-label">Portfolios</label>
                                    <div class="layui-input-block">
                                        <select id="select_p" lay-filter="portfolios">
                                            <option value="0">Select Portfolios</option>
                                            @if(!empty($portfolios))
                                                @foreach($portfolios as $portfolio)
                                                    <option value="{{ $portfolio->id }}">{{ $portfolio->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="layui-input-block">
                                        <table class="layui-table">
                                            <colgroup>
                                                <col width="150">
                                                <col width="150">
                                                <col width="200">
                                                <col width="200">
                                                <col>
                                            </colgroup>
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Job</th>
                                                <th>Time</th>
                                                <th>Tags</th>
                                                <th>Tips</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if(!empty($portfolios))
                                                @foreach($portfolios as $portfolio)
                                                    <tr class="portfolio{{$portfolio->id}}" style="display: none;">
                                                        <td>
                                                            <input type="hidden" name="portfolios[{{$portfolio->id}}][pid]" value="{{ $portfolio->id }}">
                                                            <input type="hidden" name="portfolios[{{$portfolio->id}}][pname]" value="{{ $portfolio->name }}">
                                                            <input type="checkbox" name="portfolios[{{$portfolio->id}}][check]" title="{{ $portfolio->name }}">
                                                        </td>
                                                        <td>
                                                            <textarea name="portfolios[{{$portfolio->id}}][job]" class="layui-textarea"></textarea>
                                                        </td>
                                                        <td>
                                                            <div class="layui-inline">
                                                                Start At: <input type="text" name="portfolios[{{$portfolio->id}}][start_at]" class="layui-input start_at{{$portfolio->id}}">
                                                            </div>
                                                            <div class="layui-inline">
                                                                End At:<input type="text" name="portfolios[{{$portfolio->id}}][end_at]" class="layui-input end_at{{$portfolio->id}}">
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <input name="portfolios[{{$portfolio->id}}][labels]" class="label-input"  type="hidden" value="{{old('skills')}}">
                                                            <textarea style="height: 20px;" class="layui-textarea labels">{{old('skills')}}</textarea>
                                                        </td>
                                                        <td>
                                                            <input name="portfolios[{{$portfolio->id}}][tips]" class="label-input"  type="hidden" value="">
                                                            <textarea name="portfolios[{{$portfolio->id}}][_tips]" style="height: 20px;" class="layui-textarea tips"></textarea>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <label class="layui-form-label">Experience</label>
                                    <div class="layui-input-block">
                                        <table class="layui-table">
                                            <colgroup>
                                                <col width="20%">
                                                <col width="60%">
                                                <col width="20%">
                                            </colgroup>
                                            <thead>
                                            <tr>
                                                <th>Job</th>
                                                <th>
                                                    Description
                                                </th>
                                                <th>
                                                    <button id="add_experience" style="float: right;" type="button" class="layui-btn layui-btn-normal">Add Experience</button>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody id="experience_tbody">
                                                <tr class="experience1">
                                                    <td>
                                                        <input type="hidden" name="experience_body[1][id]" value="1">
                                                        <textarea name="experience_body[1][job]" style="min-height: 18px !important;" class="layui-textarea"></textarea>
                                                    </td>
                                                    <td>
                                                        <textarea name="experience_body[1][description]" style="min-height: 18px !important;" class="layui-textarea"></textarea>
                                                    </td>
                                                    <td>
                                                        <button style="float: right;" data-experience-id="1" type="button" class="layui-btn layui-btn-danger delete-experience">Delete</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <label class="layui-form-label">Education</label>
                                    <div class="layui-input-block">
                                        <table class="layui-table">
                                            <colgroup>
                                                <col width="35%">
                                                <col width="20%">
                                                <col>
                                            </colgroup>
                                            <thead>
                                            <tr>
                                                <th>University</th>
                                                <th>Discipline</th>
                                                <th>Time</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <textarea style="min-height: 18px !important;" name="education[1][university]" class="layui-textarea"></textarea>
                                                </td>
                                                <td>
                                                    <textarea style="min-height: 18px !important;" name="education[1][discipline]" class="layui-textarea"></textarea>
                                                </td>
                                                <td>
                                                    <div class="layui-inline">
                                                        Start At: <input type="text" name="education[1][start_at]" class="layui-input edu_start_at">
                                                    </div>
                                                    <div class="layui-inline">
                                                        End At:<input type="text" name="education[1][end_at]" class="layui-input edu_end_at">
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <label class="layui-form-label">Skills</label>
                                    <div class="layui-input-block">
                                        <table class="layui-table">
                                            <colgroup>
                                                <col width="44%">
                                                <col width="44%">
                                                <col>
                                            </colgroup>
                                            <thead>
                                            <tr>
                                                <th>Skill</th>
                                                <th>
                                                    Time(months)
                                                </th>
                                                <th>
                                                    <button id="add_skill" style="float: right;" type="button" class="layui-btn layui-btn-normal">Add Skill</button>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody id="skills_tbody">
                                            <tr class="skills1">
                                                <td>
                                                    <textarea name="skills[1][name]" style="min-height: 18px !important;" class="layui-textarea"></textarea>
                                                </td>
                                                <td>
                                                    <textarea name="skills[1][time]" style="min-height: 18px !important;" class="layui-textarea"></textarea>
                                                </td>
                                                <td>
                                                    <button style="float: right;" data-skills-id="1" type="button" class="layui-btn layui-btn-danger delete-skills">Delete</button>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <label class="layui-form-label">Tools</label>
                                    <div class="layui-input-block">
                                        <input name="tools" class="label-input"  type="hidden" value="{{old('tools')}}">
                                        <textarea style="height: 20px;" class="layui-textarea labels">{{old('tools')}}</textarea>
                                    </div>
                                </div>

                                <button class="layui-btn" lay-submit >Create</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /. PAGE INNER  -->
    </div>
@endsection

@section('after_script')
    <link href="{{ asset('css/jquery.tagsinput.css') }}" rel="stylesheet" />
    <script src="{{ asset('js/jquery.tagsinput.min.js') }}"></script>
    <script>
        $(function () {
            var addSkill = $("#add_skill");
            var addExperience = $("#add_experience");
            var skillTBody = $("#skills_tbody");
            var experienceBody = $("#experience_tbody");
            layui.use('form', function(){
                var form = layui.form;

                form.on('select(portfolios)', function(data){
                    $(".portfolio"+data.value).show();
                });
            });

            layui.use('rate', function(){
                var rate = layui.rate;

                //渲染
                var ins1 = rate.render({
                    elem: '#test1'  //绑定元素
                });
            });

            @if(!empty($portfolios))
                @foreach($portfolios as $portfolio)
                    layui.use('laydate', function(){
                        var laydate = layui.laydate;

                        //执行一个laydate实例
                        laydate.render({
                            elem: '.start_at' + '{{ $portfolio->id }}' //指定元素
                            ,lang: 'en'
                        });
                    });

                    layui.use('laydate', function(){
                        var laydate = layui.laydate;

                        //执行一个laydate实例
                        laydate.render({
                            elem: '.end_at'  + '{{ $portfolio->id }}' //指定元素
                            ,lang: 'en'
                        });
                    });
                @endforeach
            @endif

            layui.use('laydate', function(){
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                    elem: '.edu_start_at' //指定元素
                    ,lang: 'en'
                });
            });

            layui.use('laydate', function(){
                var laydate = layui.laydate;

                //执行一个laydate实例
                laydate.render({
                    elem: '.edu_end_at' //指定元素
                    ,lang: 'en'
                });
            });

            layui.use('upload', function(){
                var upload = layui.upload;

                //upload logo
                upload.render({
                    elem: '#portrait_but' //绑定元素
                    ,url: '/admin/image/portrait' //上传接口
                    ,headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                    ,done: function(res){
                        $("#portrait_img").attr('src', res.url);
                        $("#portrait").val(res.url);
                    }
                    ,error: function(){
                        //请求异常回调
                    }
                });
            });


            //删除指定的下标
            Array.prototype.removeByTag = function(Tag) {
                for(var i=0; i<this.length; i++) {
                    if(this[i] == Tag) {
                        this.splice(i, 1);
                        break;
                    }
                }
            };

            //过滤特殊字符
            function stripscript(s) {
                var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]")
                var rs = "";
                for (var i = 0; i < s.length; i++) {
                    rs = rs+s.substr(i, 1).replace(pattern, '');
                }
                return rs;
            }

            //添加标签
            function onAddTagInSkills(tag) {
                //非法字符过滤
                tag = stripscript(tag);
                var post_skills = $(this).prev();
                var post_skills_val = post_skills.val();
                post_skills.val(post_skills_val + ',' + tag);
            }

            //删除标签
            function onRemoveTagInSkills(tag) {
                //非法字符过滤
                tag = stripscript(tag);
                var post_skills = $(this).prev();
                var post_skills_val = post_skills.val();
                var post_skills_arr = post_skills_val.split(",");// 在每个逗号(,)处进行分解.
                post_skills_arr.removeByTag(tag);
                post_skills.val(post_skills_arr.join(','));
            }

            //修改标签 -- 暂不支持
            function onChangeTagInSkills(input,tag) {}

            var labels = $(".labels");
            labels.tagsInput({
                'interactive':true, //是否允许添加标签，false为阻止
                'defaultText':'', //默认文字
                'onAddTag':onAddTagInSkills, //增加标签的回调函数
                'onRemoveTag':onRemoveTagInSkills, //删除标签的回调函数
                'onChange' : onChangeTagInSkills, //改变一个标签时的回调函数
                'removeWithBackspace' : true, //是否允许使用退格键删除前面的标签，false为阻止
                'minChars' : 1, //每个标签的小最字符
                'maxChars' : 20, //每个标签的最大字符，如果不设置或者为0，就是无限大
                'placeholderColor' : '#FF5722' //设置defaultText的颜色
            });

            var tips = $(".tips");
            tips.tagsInput({
                'interactive':true, //是否允许添加标签，false为阻止
                'defaultText':'', //默认文字
                'onAddTag':onAddTagInSkills, //增加标签的回调函数
                'onRemoveTag':onRemoveTagInSkills, //删除标签的回调函数
                'onChange' : onChangeTagInSkills, //改变一个标签时的回调函数
                'removeWithBackspace' : true, //是否允许使用退格键删除前面的标签，false为阻止
                'minChars' : 1, //每个标签的小最字符
                //'maxChars' : 20, //每个标签的最大字符，如果不设置或者为0，就是无限大
                'placeholderColor' : '#FF5722' //设置defaultText的颜色
            });

            $(".delete-skills").bind('click', function () {
                var skillId = $(this).data('skills-id');
                $('.skills' + skillId).remove();
            });

            addSkill.click(function () {
                var i = Math.random();
                var randomStr = (i+'').substring(2, 10);
                var _html = '<tr class="skills'+randomStr+'"><td>'+
                        '<textarea name="skills['+randomStr+'][name]" style="min-height: 18px !important;" class="layui-textarea"></textarea>'+
                        '</td><td>'+
                        '<textarea name="skills['+randomStr+'][time]" style="min-height: 18px !important;" class="layui-textarea"></textarea></td>'+
                        '<td> <button style="float: right;" data-skills-id="'+randomStr+'" type="button" class="layui-btn layui-btn-danger delete-skills">Delete</button></td></tr>';
                skillTBody.append(_html);

                $(".delete-skills").bind('click', function () {
                    var skillId = $(this).data('skills-id');
                    $('.skills' + skillId).remove();
                });
            });


            $(".delete-experience").bind('click', function () {
                var experienceId = $(this).data('experience-id');
                $('.experience' + experienceId).remove();
            });
            addExperience.click(function () {
                var i = Math.random();
                var randomStr = (i+'').substring(2, 10);
                var _html = '<tr class="experience'+randomStr+'"><td><input type="hidden" name="experience_body['+randomStr+'][id]" value="'+randomStr+'">'+
                        '<textarea name="experience_body['+randomStr+'][job]" style="min-height: 18px !important;" class="layui-textarea"></textarea>'+
                        '</td><td>'+
                        '<textarea name="experience_body['+randomStr+'][description]" style="min-height: 18px !important;" class="layui-textarea"></textarea></td>'+
                        '<td> <button style="float: right;" data-experience-id="'+randomStr+'" type="button" class="layui-btn layui-btn-danger delete-experience">Delete</button></td></tr>';
                experienceBody.append(_html);

                $(".delete-experience").bind('click', function () {
                    var experienceId = $(this).data('experience-id');
                    $('.experience' + experienceId).remove();
                });
            });
        });
    </script>
@endsection