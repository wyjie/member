@extends('layouts.admin')
@section('before_style')
    <style>
        .image-url {
            margin-top: 20px;
            margin-left: 20px;
            height: 150px;
        }

        .image-but {
            margin-top: 20px;
            margin-left: 20px;
        }
    </style>
@endsection

@section('content')
    <div id="page-wrapper">
        <div id="page-inner">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="page-head-line">Edit Portfolio</h2>
                </div>
            </div>
            <!-- /. ROW  -->
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            Edit Portfolio
                        </div>
                        <div class="panel-body">
                            <form class="layui-form" method="POST" action="{{ url('admin/portfolio/update') }}">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">Name</label>
                                    <div class="layui-input-block">
                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                        <input type="hidden" name="id" value="{{ $data->id }}">
                                        <input type="text" name="name" value="{{$data->name}}" required  lay-verify="required" placeholder="Please enter name" autocomplete="off" class="layui-input">
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <label class="layui-form-label">Link</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="link" value="{{$data->link}}" required  lay-verify="required" placeholder="Please enter link. http://...." autocomplete="off" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">About</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="about" value="{{$data->about}}"  placeholder="Please enter about." autocomplete="off" class="layui-input">
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <label class="layui-form-label">Logo</label>
                                    <div class="layui-input-block">
                                        <input id="logo"  class="form-control" name="logo" value="{{ $data->logo }}"  type="hidden">
                                        <img class="image-url" id="logo_img" src="{{ $data->logo }}" alt="">
                                        <div>
                                            <button type="button" class="layui-btn image-but" id="logo_but">
                                                <i class="layui-icon">&#xe67c;</i>upload logo
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="layui-form-item">
                                    <label class="layui-form-label">Image</label>
                                    <div class="layui-input-block">
                                        <input id="image" class="form-control" name="image"  value="{{ $data->image }}" type="hidden">
                                        <img class="image-url" id="image_img" src="{{ $data->image }}" alt="">
                                        <div>
                                            <button type="button" class="layui-btn image-but" id="image_but">
                                                <i class="layui-icon">&#xe67c;</i>upload image
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                {{--<div class="layui-form-item">--}}
                                    {{--<label class="layui-form-label">list</label>--}}
                                    {{--<div id="lists">--}}
                                        {{--@if(!empty($data->list) && !empty($data->list[0]))--}}
                                            {{--@foreach($data->list as $item)--}}
                                                {{--<div class="layui-input-inline" style="margin-left: 30px">--}}
                                                    {{--<input type="text" value="{{ $item }}" name="list[]"  autocomplete="off" class="layui-input">--}}
                                                {{--</div>--}}
                                            {{--@endforeach--}}
                                        {{--@endif--}}
                                    {{--</div>--}}
                                    {{--<div class="layui-input-inline list-add-but">--}}
                                        {{--<button type="button" class="layui-btn"><i class="layui-icon">&#xe654;</i> add</button>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">Description</label>
                                    <div class="layui-input-block">
                                        <textarea id="description" name="description" placeholder="Please enter body..." class="layui-textarea">{{ $data->description }}</textarea>
                                    </div>
                                </div>

                                <button class="layui-btn" lay-submit >Update</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- /. PAGE INNER  -->
    </div>
@endsection

@section('after_script')
    <script>
        $(function () {

            var lists = $("#lists");

            //add list
            $(".list-add-but").on('click', function () {
                //删除自己，然后添加一个新的
                var _html =
                        '<div class="layui-input-inline" style="margin-left: 30px">'+
                        '<input type="text" name="list[]"  autocomplete="off" class="layui-input">'+
                        '</div>';
                lists.append(_html);

            });

            layui.use('upload', function(){
                var upload = layui.upload;

                //upload logo
                upload.render({
                    elem: '#logo_but' //绑定元素
                    ,url: '/admin/image/logo' //上传接口
                    ,headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                    ,done: function(res){
                        $("#logo_img").attr('src', res.url);
                        $("#logo").val(res.url);
                    }
                    ,error: function(){
                        //请求异常回调
                    }
                });


                //upload image
                upload.render({
                    elem: '#image_but' //绑定元素
                    ,url: '/admin/image/image' //上传接口
                    ,headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                    ,done: function(res){
                        $("#image_img").attr('src', res.url);
                        $("#image").val(res.url);
                    }
                    ,error: function(){
                        //请求异常回调
                    }
                });
            });
        });
    </script>
@endsection