<nav class="navbar-default navbar-side" role="navigation" id="path" data-path="{{ $_SERVER['REQUEST_URI'] }}">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
            <li>
                <div class="user-img-div">
                        <img src="{{ asset('image/tx.jpg') }}" class="img-thumbnail" />

                    <div class="inner-text">
                        {{ Auth::user()->email }}
                        <br />
                        <small>Rome was not built in a day. </small>
                    </div>
                </div>

            </li>
                <!-- dashboard start -->
                {{--<li>--}}
                    {{--<a id="home"  href="{{ url('/home') }}"><i class="fa fa-dashboard"></i>Dashboard</a>--}}
                {{--</li>--}}
                <!-- dashboard end -->

                <li>
                    <a class="side" id="user" href="{{ url('admin/user') }}"><i class="fa fa-flash"></i>User Management</a>
                </li>
                <li>
                    <a class="side" id="portfolio"  href="{{ url('admin/portfolio') }}"><i class="fa fa-code"></i>Portfolio Management</a>
                </li>

            <!-- dictionary start -->
            {{--<li >--}}
                {{--<a class="side" id="tree" href="{{ url('tree') }}"><i class="fa fa-anchor "></i>Dictionary</a>--}}
            {{--</li>--}}
            {{--<!-- dictionary end -->--}}
            {{--<!-- role start -->--}}
            {{--<li>--}}
                {{--<a class="side" id="role" href="{{ url('role') }}"><i class="fa fa-anchor "></i>Role</a>--}}
            {{--</li>--}}
            <!-- role end -->
        </ul>
    </div>

</nav>
<script>
//    $(function () {
//        var path = $("#path").data('path');
//        path = path.substring(1);
//
//        $(".active-menu").removeClass('active-menu');
//        $("#"+path).addClass('active-menu');
//    })
</script>