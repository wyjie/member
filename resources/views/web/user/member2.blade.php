<!DOCTYPE html>
<!-- saved from url=(0045)https://t1.imgn.to/team-member/fullstack-001/ -->
<html class="light-skin  smooth-scroll-false js flexbox flexboxlegacy canvas canvastext postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache" lang="en-US" prefix="og: http://ogp.me/ns#"><!--<![endif]--><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <!-- META TAGS -->

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- LINK TAGS -->


    <link rel="shortcut icon" href="{{ asset('image/imaginato-favicon.png') }}" type="image/x-icon">
    <link href="{{ asset('css/member1.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/member2.css') }}" rel="stylesheet" type="text/css">


    <link type="text/css" media="screen" href="{{ asset('css/autoptimize_c41f90b214e7f4303661a54b2237f1f0.css?version=1.0.1') }}" rel="stylesheet">
    <link type="text/css" media="all" href="{{ asset('css/autoptimize_d79ff9d613735201e60036e00d8b69cd.css?version=1.0.1') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/font-awesome.css') }}">
    <meta name="description" content="Imaginato works with companies to build exciting new mobile and online technologies, connecting people to services, products, and each other in innovative new ways.">
    <meta name="keywords" content="ECommerce, Mobile App, Magento, Marketplace, iOS, Android, Outsourcing, Offshoring">

    <meta property="og:locale" content="en_US">
    <meta property="og:type" content="website">
    <meta property="og:title" content="Imaginato">
    <meta property="og:description" content="Imaginato works with companies to build exciting new mobile and online technologies, connecting people to services, products, and each other in innovative new ways.">
    <meta property="og:url" content="http://www.imaginato.com/">
    <meta property="og:site_name" content="Imaginato">
    <meta property="og:image" itemprop="image" content="http://www.imaginato.com/wp-content/uploads/2017/05/contact.jpg">
    <meta property="og:image:width" content="1980">
    <meta property="og:image:height" content="688">

    <!--Check IP for US-->
    <script async="" src="https://www.google-analytics.com/analytics.js" style="color: rgb(0, 0, 0);"></script><script type="text/javascript">
        var current_url = 'https://t1.imgn.to/team-member/fullstack-001/';
        var home = 'https://t1.imgn.to';
        var isHome = '';
        function customRequest(){var e={},n=!!window.XDomainRequest;if(n){var o=new window.XDomainRequest;o.open("GET","http://ipinfo.io/json",!1),o.send(null),200==o.status&&(e=JSON.parse(s.responseText))}else{var s=new XMLHttpRequest;s.open("GET","http://ipinfo.io/json",!1),s.send(null),200==s.status&&(e=JSON.parse(s.responseText))}return e}if(current_url.indexOf("/what-we-do/")>-1||isHome){var response=customRequest();current_url.indexOf("/what-we-do/")>-1&&response.country&&"US"===response.country&&(window.location.href=home+"/what-we-do-tech/")}
    </script>
    <!--Check IP for US end -->


    <!-- This site is optimized with the Yoast SEO plugin v3.4.2 - https://yoast.com/wordpress/plugins/seo/ -->
    <meta name="description" content="Full stack remote developer, ready to start in 1-2 weeks.">
    <meta name="robots" content="noodp">
    <!-- / Yoast SEO plugin. -->

    <link rel="dns-prefetch" href="https://s.w.org/">
    <link rel="alternate" type="application/rss+xml" title="Imaginato » Feed" href="https://t1.imgn.to/feed/">
    <link rel="alternate" type="application/rss+xml" title="Imaginato » Comments Feed" href="https://t1.imgn.to/comments/feed/">
    <link rel="alternate" type="application/rss+xml" title="Imaginato » Comments Feed" href="https://t1.imgn.to/team-member/fullstack-001/feed/">
    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/svg\/","svgExt":".svg","source":{"concatemoji":"{{ asset('js/wp-emoji-release.min.js') }}"}};
        !function(a,b,c){function d(a){var c,d,e,f,g,h=b.createElement("canvas"),i=h.getContext&&h.getContext("2d"),j=String.fromCharCode;if(!i||!i.fillText)return!1;switch(i.textBaseline="top",i.font="600 32px Arial",a){case"flag":return i.fillText(j(55356,56806,55356,56826),0,0),!(h.toDataURL().length<3e3)&&(i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,65039,8205,55356,57096),0,0),c=h.toDataURL(),i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,55356,57096),0,0),d=h.toDataURL(),c!==d);case"diversity":return i.fillText(j(55356,57221),0,0),e=i.getImageData(16,16,1,1).data,f=e[0]+","+e[1]+","+e[2]+","+e[3],i.fillText(j(55356,57221,55356,57343),0,0),e=i.getImageData(16,16,1,1).data,g=e[0]+","+e[1]+","+e[2]+","+e[3],f!==g;case"simple":return i.fillText(j(55357,56835),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode8":return i.fillText(j(55356,57135),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode9":return i.fillText(j(55358,56631),0,0),0!==i.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity","unicode9"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>
    <script src="{{ asset('js/wp-emoji-release.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="{{ asset('js/jquery-1.10.2.js') }}"></script>
    <script>jQueryWP = jQuery;</script>
    <link rel="https://api.w.org/" href="https://t1.imgn.to/wp-json/">
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://t1.imgn.to/xmlrpc.php?rsd">
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://t1.imgn.to/wp-includes/wlwmanifest.xml">
    <meta name="generator" content="WordPress 4.6.11">
    <link rel="shortlink" href="https://t1.imgn.to/?p=4760">
    <link rel="alternate" type="application/json+oembed" href="https://t1.imgn.to/wp-json/oembed/1.0/embed?url=https%3A%2F%2Ft1.imgn.to%2Fteam-member%2Ffullstack-001%2F">
    <link rel="alternate" type="text/xml+oembed" href="https://t1.imgn.to/wp-json/oembed/1.0/embed?url=https%3A%2F%2Ft1.imgn.to%2Fteam-member%2Ffullstack-001%2F&amp;format=xml">
    <link hreflang="en" href="https://t1.imgn.to/en/team-member/fullstack-001/" rel="alternate">
    <link hreflang="zh" href="https://t1.imgn.to/cn/team-member/fullstack-001/" rel="alternate">
    <link hreflang="x-default" href="https://t1.imgn.to/team-member/fullstack-001/" rel="alternate">
    <meta name="generator" content="qTranslate-X 3.4.6.8">
</head>
<body class="single single-team-member postid-4760 micron-child">
<!-- wrap start -->
<div id="wrap" class="full">


    <header class="header with-transition header-top-false responsive-true height-80 attachment-scroll">

        <div class="search">
            <div class="container">
                <form action="https://t1.imgn.to/" method="get">
                    <fieldset>
                        <input type="text" name="s" id="s" placeholder="Search for ...">
                        <input type="submit" id="searchsubmit" value="Search">
                    </fieldset>
                </form>                    <div class="search-off">&nbsp;</div>
            </div>
        </div>


        <div class="header-content">
            <div class="container ninzio-clearfix">

                <div class="logo">
                    <a href="/" title="Imaginato">
                        <img src="{{ asset('image/imaginato_logo.png') }}" alt="Imaginato">
                    </a>
                </div>

                <div class="inline-clear ninzio-clearfix">&nbsp;</div>
                <div class="search-toggle"><i class="fa fa-search"></i></div>
                <div class="responsive-menu-toggle"><span></span><!--<i class="fa fa-th-list"></i>--></div>

                <nav class="header-menu ninzio-clearfix">
                    <ul id="header-menu" class="menu"><li id="menu-item-3301" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3301"><a href="https://t1.imgn.to/services/">Services</a>
                            <ul class="sub-menu">
                                <li id="menu-item-3849" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3849"><a href="https://t1.imgn.to/services/product-consulting/">Product Consulting</a></li>
                                <li id="menu-item-3302" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3302"><a href="https://t1.imgn.to/services/apps/">Mobile App Development</a></li>
                                <li id="menu-item-3303" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3303"><a href="https://t1.imgn.to/services/webdevelopment/">Web Development</a></li>
                                <li id="menu-item-3306" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3306"><a href="https://t1.imgn.to/services/managed-services/">Managed Services</a></li>
                                <li id="menu-item-3305" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3305"><a href="https://t1.imgn.to/services/dedicated-engineers/">Dedicated Engineers</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-3309" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3309"><a href="https://t1.imgn.to/work/">Work</a></li>
                        <li id="menu-item-3310" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3310"><a href="https://t1.imgn.to/trending/">Trending</a></li>
                        <li id="menu-item-3307" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3307"><a href="https://t1.imgn.to/about/">About</a></li>
                        <li id="menu-item-3308" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3308"><a href="https://t1.imgn.to/contact-us/">Contact</a></li>
                        <li id="menu-item-4574" class="qtranxs-lang-menu qtranxs-lang-menu-en menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-4574"><a title="English" href="https://t1.imgn.to/team-member/fullstack-001/#">English</a>
                            <ul class="sub-menu">
                                <li id="menu-item-4575" class="qtranxs-lang-menu-item qtranxs-lang-menu-item-en menu-item menu-item-type-custom menu-item-object-custom menu-item-4575 current-menu-item"><a title="English" href="https://t1.imgn.to/en/team-member/fullstack-001/"><img src="{{ asset('image/gb.png') }}" alt="English">&nbsp;English</a></li>
                                <li id="menu-item-4576" class="qtranxs-lang-menu-item qtranxs-lang-menu-item-cn menu-item menu-item-type-custom menu-item-object-custom menu-item-4576"><a title="中文" href="https://t1.imgn.to/cn/team-member/fullstack-001/"><img src="{{ asset('image/cn.png') }}" alt="中文">&nbsp;中文</a></li>
                            </ul>
                        </li>
                    </ul>                    </nav>

            </div>
        </div>

    </header>

    <div class="header2">

        <div class="header-content">
            <div class="container ninzio-clearfix">

                <div class="logo-black">
                    <a href="https://t1.imgn.to/" title="Imaginato">
                        <img src="{{ asset('image/imaginato_logo.png') }}" alt="Imaginato">
                    </a>
                </div>

                <nav class="header-menu ninzio-clearfix">
                    <ul id="header-menu" class="menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-3301"><a href="https://t1.imgn.to/services/">Services</a>
                            <ul class="sub-menu">
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3849"><a href="https://t1.imgn.to/services/product-consulting/">Product Consulting</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3302"><a href="https://t1.imgn.to/services/apps/">Mobile App Development</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3303"><a href="https://t1.imgn.to/services/webdevelopment/">Web Development</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3306"><a href="https://t1.imgn.to/services/managed-services/">Managed Services</a></li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3305"><a href="https://t1.imgn.to/services/dedicated-engineers/">Dedicated Engineers</a></li>
                            </ul>
                        </li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3309"><a href="https://t1.imgn.to/work/">Work</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3310"><a href="https://t1.imgn.to/trending/">Trending</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3307"><a href="https://t1.imgn.to/about/">About</a></li>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3308"><a href="https://t1.imgn.to/contact-us/">Contact</a></li>
                        <li class="qtranxs-lang-menu qtranxs-lang-menu-en menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-4574"><a title="English" href="https://t1.imgn.to/team-member/fullstack-001/#">English</a>
                            <ul class="sub-menu">
                                <li class="qtranxs-lang-menu-item qtranxs-lang-menu-item-en menu-item menu-item-type-custom menu-item-object-custom menu-item-4575 current-menu-item"><a title="English" href="https://t1.imgn.to/en/team-member/fullstack-001/"><img src="{{ asset('image/gb.png') }}" alt="English">&nbsp;English</a></li>
                                <li class="qtranxs-lang-menu-item qtranxs-lang-menu-item-cn menu-item menu-item-type-custom menu-item-object-custom menu-item-4576"><a title="中文" href="https://t1.imgn.to/cn/team-member/fullstack-001/"><img src="{{ asset('image/cn.png') }}" alt="中文">&nbsp;中文</a></li>
                            </ul>
                        </li>
                    </ul>                    </nav>

            </div>
        </div>
        <script>
            var oLanguage = document.querySelectorAll('.qtranxs-lang-menu');
            var oLanguageTitle = document.querySelectorAll('.qtranxs-lang-menu>a');
            var oLanguageItem1 = document.querySelectorAll('.header .qtranxs-lang-menu ul li')
            var oLanguageItem2 = document.querySelectorAll('.header2 .qtranxs-lang-menu ul li')
            for (var i=0;i<oLanguage.length;i++) {
                oLanguage[i].classList.remove('current-menu-parent')
                oLanguageTitle[i].innerText = oLanguageTitle[i].title;
            }

            if (!document.querySelector('[lang="zh-CN"]')) {
                oLanguageItem1[0].classList.add('current-menu-item')
                oLanguageItem2[0].classList.add('current-menu-item')

            }
        </script>
    </div>

    <div class="page-content-container">  <div class="main-banner">

            <img src="{{ asset('image/banner-img.jpg') }}" alt="">

        </div> <!-- profile-pic -->
        <div class="main-wrapper">
            <div class="profile-section">

                <div class="profile-pic">
                    <img width="237px" height="273px" src="{{ $user->portrait }}">
                </div> <!-- profile-pic -->

                <div class="profile-content">
                    <h1>{{ $user->first_name }}<span><i class="fa fa-check-circle-o" aria-hidden="true"></i>
 Profile Verified
 </span> </h1>
                    <div class="divider">
                        <h5>{{ $user->member_type }}</h5><div class="some_detail"><i class="fa fa-dot-circle-o" aria-hidden="true"></i>
                            {{ $user->experience }} Years Exp.
                            <span><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $user->location }}</span></div> <!-- some_detail -->
                    </div> <!-- divider-->
                    <div class="profile_description">
                        <p>
                            {{ $user->description }}.
                        </p>
                        <ul>
                            @if(!empty($skills))
                                @foreach($skills as $skill)
                                    <li><a href="javascript:;">{{ $skill->label_name }}</a></li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                    <!-- profile_description  -->
                </div> <!-- profile-content  -->
            </div> <!-- profile-section -->


            <div class="quote_section">

                <h3>{{ $user->first_name }} <span>
 Available By
 : <i class="fa fa-calendar" aria-hidden="true"></i>
                        {{ $time }} 1</span></h3>
                <!--<a href="#">Request A Quote</a>-->
                <a href="mailto:staffing@imaginato.com?subject=New%20staffing%20inquiry">Check availability and rates</a>
            </div> <!-- quote_section -->







            <div class="amazing-section">
                <div class="preferred_environment_block"><h3>Preferred Environment <span>{{ $user->preferred_environment }}</span></h3>
                </div>				<!-- preferred_environment_block -->

                <div class="most_amazing_block"><p><strong>the most amazing…</strong> …{{ $user->amazing }}</p>
                </div><!-- amazing-section -->                </div>



            <!--<div class="portfolio-section">
            </div> <!-- portfolio-section -->

            <div class="portfolio-section">


                <h3>PORTFOLIO</h3>
                <ul>
                    @if(!empty($portfolios))
                        @foreach($portfolios as $key => $portfolio)
                            @if($key < 3)
                                <li>
                                    <a href="{{ $portfolio->link }}"  target="_blank">
                                        <div style="line-height: 360px;" class="thumb-img">
                                            <img  style="width: 360px;display: inline-block;vertical-align: middle;" src="{{ $portfolio->image }}">
                                        </div> <!-- thumb-img -->
                                        <div style="position: relative;">
                                            <div class="content-image-over">
                                                <h4>{{ $portfolio->name }}</h4>
                                                <p>{{ $portfolio->about }}</p>
                                            </div>
                                        </div>
                                    </a>

                                </li>
                            @endif
                        @endforeach
                    @endif

                </ul>
            </div>



            <div class="experience-section">
                <h3>Work History</h3>
                @if(!empty($portfolios))
                    @foreach($portfolios as $portfolio)
                        <div class="experience-inner">
                            <div class="left-logo">
                                <img height="90px" width="90px" src="{{ $portfolio->logo }}">
                            </div> <!-- left-logo -->
                            <div class="experience-content">
                                <h1>{{ $portfolio->job }}</h1>
                                <div class="divider">
                                    <h5>{{ $portfolio->about }}</h5>
                                    <div class="some_detail mob">{{ date('Y', strtotime($portfolio->start_at)) . ' - ' . date('Y', strtotime($portfolio->end_at)) }}</div> <!-- some_detail -->
                                </div> <!-- divider-->
                                <div class="experience_description">
                                    <p>{{ $portfolio->description }}<br>
                                        &nbsp;<br>
                                    @foreach($portfolio->list as $item)
                                        @if(!empty($item))
                                            {{'-' . $item }}<br>
                                        @endif
                                    @endforeach
                                    <div class="technologies_block">
                                        <h5>Technologies</h5>
                                        <ul>
                                            @foreach($portfolio->labels as $plabel)
                                                @if(!empty($plabel))
                                                    <li><a href="javascript:;">{{ $plabel }}</a></li>
                                                @endif
                                            @endforeach
                                        </ul>

                                    </div><!-- technologies_block  -->

                                </div>
                                <!-- experience-description  -->
                            </div><!-- experience-content  -->

                        </div>
                    @endforeach
                @endif
            </div> <!-- experience-section -->

            <div class="education-section bottom_experience_block">

                <h3>EXPERIENCE</h3>

                <div class="eduction-content">
                    <div class="full-text">
                        <div class="somefull_detail">
                            @if(!empty($experience_body))
                                @foreach($experience_body as $item)
                                    <p>
                                        <strong>
                                            <span style="font-size:18px !important; line-height:26px !important;" class="ninzio-font-size">{{ $item->job }}</span>
                                        </strong><br>
                                        {{ $item->description }}
                                    </p>
                                    <div class="ninzio-gap inline-ninzio-clearfix" style="height:20px">&nbsp;</div><p></p>
                                @endforeach
                            @endif
                        </div> <!-- some_detail -->
                    </div> <!-- divider-->
                </div> <!-- eduction-content -->



            </div> <!-- education-section -->

            <div class="education-section">
                <!--?php// echo $educationmaintitle; ?-->

                <h3>EDUCATION</h3>


                @if(!empty($edus))
                    @foreach($edus as $ude)
                        <div class="eduction-content">
                            <h1>{{ $ude->science }}</h1>
                            <div class="divider">
                                <h5>{{ $ude->edu_name }}</h5>
                                <div class="some_detail">{{ date('Y', strtotime($ude->start_at)) . ' - ' . date('Y', strtotime($ude->end_at)) }}</div> <!-- some_detail -->
                            </div> <!-- divider-->
                        </div> <!-- eduction-content -->
                    @endforeach
                @endif


            </div> <!-- education-section -->


            <div class="skill-tool-section">

                <div class="skills-section">
                    <h3>SKILLS</h3>

                    <table>
                        <tbody>
                        @if(!empty($skills))
                            @foreach($skills as $skill)
                                <tr>
                                    <td><i class="fa fa-check" aria-hidden="true"></i>{{ $skill->label_name }}</td>
                                    <td>{{ $skill->time }} months</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div> <!-- skills-section -->
                <div class="tool-section">
                    <h3>TOOLS</h3>

                    <table>
                        <tbody>
                        @if(!empty($user->tools))
                            @foreach($user->tools as $tool)
                                @if(!empty($tool))
                                    <tr>
                                        <td><i class="fa fa-check" aria-hidden="true"></i>{{ $tool }}</td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div> <!-- tool-section -->



            </div> <!-- skill-tool-section -->


        </div> <!-- main-wrapper -->


        <div class="bottom-gray-section">

            <div class="main-wrapper">

                <div class="bottom-quote-section">
                    <h2>We can have your new developer ready to go in 1-2 weeks.</h2>
                    <a class="bottom_quote" href="mailto:staffing@imaginato.com?subject=New%20staffing%20inquiry">Check availability and rates</a>
                </div> <!-- bottom-quote-section-->

            </div> <!-- main-wrapper -->


        </div> <!-- bottom-gray-section -->





    </div>

    <!-- page-content-container end -->



    <!-- footer start -->

    <footer class="footer">

        <div class="footer-widget-area-wrap">

            <aside class="footer-widget-area widget-area columns-4">
                <div class="container ninzio-clearfix">
                    <section id="nav_menu-2" class="widget widget_nav_menu"><div class="menu-footer-container"><ul id="menu-footer" class="menu"><li id="menu-item-3857" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3857"><a href="https://t1.imgn.to/">Home</a></li>
                                <li id="menu-item-4237" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4237"><a href="https://t1.imgn.to/services/">Services</a></li>
                                <li id="menu-item-2611" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2611"><a href="https://t1.imgn.to/work/">Work</a></li>
                                <li id="menu-item-3726" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3726"><a href="https://t1.imgn.to/trending/">Trending</a></li>
                                <li id="menu-item-3727" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3727"><a href="https://t1.imgn.to/about/">About</a></li>
                                <li id="menu-item-2615" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2615"><a href="https://t1.imgn.to/contact-us/">Contact</a></li>
                                <li id="menu-item-2616" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2616"><a href="https://t1.imgn.to/privacy/">Privacy Policy</a></li>
                            </ul></div></section><section id="twitter-2" class="widget widget_twitter"><h6 class="widget_title">Twitter Feeds</h6>
                        <div class="twitter">
                            <ul>
                                <li>
                                    <a target="_blank" href="http://twitter.com/imgnto/statuses/994004101636685824">
                                        https://t.co/TVHxgEiW70                                </a>
                                    <a class="tweet-time" target="_blank" href="http://twitter.com/imgnto/statuses/994004101636685824">5 months ago </a>
                                </li>
                                <li>
                                    <a target="_blank" href="http://twitter.com/imgnto/statuses/993780017141297152">
                                        Alibaba on a shopping spree with Rocket Internet https://t.co/TVHxgEiW70                                </a>
                                    <a class="tweet-time" target="_blank" href="http://twitter.com/imgnto/statuses/993780017141297152">5 months ago </a>
                                </li>
                                <li>
                                    <a target="_blank" href="http://twitter.com/imgnto/statuses/975884641340096512">
                                        Alibaba doubles down on Lazada with fresh $2B investment and new CEO https://t.co/rBW6BiPf7Q                                </a>
                                    <a class="tweet-time" target="_blank" href="http://twitter.com/imgnto/statuses/975884641340096512">7 months ago </a>
                                </li>
                            </ul>
                        </div>

                    </section><section id="text-3" class="widget widget_text"><h6 class="widget_title">Linkedin Feeds</h6>			<div class="textwidget"><div class="linkedin-footer">

                            </div></div>
                    </section><section id="text-2" class="widget widget_text">			<div class="textwidget"><div class="widget-follow">
                                <h6 class="widget_title">Follow Us</h6>
                                <a href="http://twitter.com/imgnto" title="Twitter" target="_blank" class="tw-btn"></a>
                                <a href="https://www.linkedin.com/company/imaginato" title="LinkedIn" target="_blank" class="li-btn"></a>
                            </div>
                            <div class="widget-contact">
                                <h6 class="widget_title">Contact Us</h6>
                                <p>
                                    Time Square Plaza, Suite 2706<br>
                                    No. 52 Hong Kong Middle Rd<br>
                                    Qingdao, Shandong, 266071<br>
                                    China
                                </p>
                                <p>contact@imaginato.com</p>
                                <p>0532-66775745</p>
                            </div></div>
                    </section>			</div>
            </aside>
        </div>

        <div class="footer-content">

            <div class="container ninzio-clearfix">


                <div class="social-links">
















                </div>


                <div class="footer-info">


                    Copyright © 2017 Imaginato. All rights reserved.

                </div>

            </div>

        </div>

    </footer>

    <!-- footer end -->






</div>

<!-- wrap end -->

<div id="toTop" style="display: block;"><span id="toTopHover"></span>&nbsp;</div>



<!-- =============================== OLD BROWSER MESSAGE =============================== -->



<div class="old-browser alert warning">

    <div class="alert-message">

        <h2>Your browser is out of date. It has security vulnerabilities and may not display all features on this site and other sites.</h2>

        <p>Please update your browser using one of modern browsers (Google Chrome, Opera, Firefox, IE 10).</p>

    </div>

    <span class="close-alert">X</span>

</div>




<!-- =============================== BLOG MASONRY ============================== -->




<script>                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-83554302-1', 'imaginato.com');
    ga('send', 'pageview');            </script>


<script type="text/javascript">
    /* <![CDATA[ */
    var _wpcf7 = {"loaderUrl":"https:\/\/t1.imgn.to\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif","sending":"Sending ...","cached":"1"};
    /* ]]> */
</script>



















<script>
    document.body.addEventListener('touchstart', function(){});
    window.onload = function() {
        jQuery('[lang="zh-CN"] .footer-info').html('© 2017 Imaginato. 版权所有');
    }
</script>

<script type="text/javascript" defer="" src="{{asset('js/autoptimize_4ac9e830b9625fa0101384bd36034fbb.js')}}"></script>



<!-- Dynamic page generated in 2.673 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2018-10-05 14:21:43 -->

<!-- super cache --><a href="https://t1.imgn.to/team-member/fullstack-001/#" id="toTop">To Top</a><div id="cboxOverlay" style="display: none;"></div><div id="colorbox" class="" role="dialog" tabindex="-1" style="display: none;"><div id="cboxWrapper"><div><div id="cboxTopLeft" style="float: left;"></div><div id="cboxTopCenter" style="float: left;"></div><div id="cboxTopRight" style="float: left;"></div></div><div style="clear: left;"><div id="cboxMiddleLeft" style="float: left;"></div><div id="cboxContent" style="float: left;"><div id="cboxTitle" style="float: left;"></div><div id="cboxCurrent" style="float: left;"></div><button type="button" id="cboxPrevious"></button><button type="button" id="cboxNext"></button><button id="cboxSlideshow"></button><div id="cboxLoadingOverlay" style="float: left;"></div><div id="cboxLoadingGraphic" style="float: left;"></div></div><div id="cboxMiddleRight" style="float: left;"></div></div><div style="clear: left;"><div id="cboxBottomLeft" style="float: left;"></div><div id="cboxBottomCenter" style="float: left;"></div><div id="cboxBottomRight" style="float: left;"></div></div></div><div style="position: absolute; width: 9999px; visibility: hidden; display: none;"></div></div></body></html>