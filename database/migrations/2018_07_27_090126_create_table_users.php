<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('password', 100)->default('');
            $table->string('email', 50)->default('')->unique()->comment('邮箱');
            $table->string('mobile', 20)->default('')->unique()->comment('手机号');
            $table->string('last_name', 10)->default('')->comment('last name');
            $table->string('first_name', 10)->default('')->comment('first name');
            $table->string('full_name', 20)->default('')->comment('全名');
            $table->string('key', 50)->default('')->comment('');
            $table->tinyInteger('gender')->default(0)->comment('性别0女1男');
            $table->string('member_type', 100)->default('')->comment('人员类型:pm, php, java, ios...');
            $table->decimal('experience', 5,1)->default(1.0)->comment('工作经验　单位:年 可以使小数例如 1.5年');
            $table->string('portrait', 255)->default('')->comment('头像');
            $table->string('location', 100)->default('chain')->comment('工作地点');
            $table->string('tools', 255)->default('')->comment('');
            $table->tinyInteger('profile_verified')->default(1)->comment('是否通过认证');
            $table->text('description')->comment('描述');
            $table->boolean('deleted')->default(0)->comment('禁用状态');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
