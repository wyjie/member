<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePortfolioLabels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_labels', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('portfolio_id')->default(0)->comment('portfolio id');
            $table->tinyInteger('label_type')->default(0)->comment('label type');
            $table->unsignedInteger('label_id')->default(0)->comment('label id');
            $table->string('label_name', 50)->default('')->comment('label name');
            $table->timestamps();
            $table->index(['portfolio_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolio_labels');
    }
}
