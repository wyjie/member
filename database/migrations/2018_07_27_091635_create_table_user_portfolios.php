<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserPortfolios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_portfolios', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->default(0)->comment('用户id');
            $table->unsignedInteger('portfolio_id')->default(0)->comment('label id');
            $table->dateTime('start_at')->default('2018-01-01 00:00:00')->comment('开始时间');
            $table->dateTime('end_at')->default('2018-01-01 00:00:00')->comment('结束时间');
            $table->string('portfolio_name', 50)->default('')->comment('');
            $table->string('job', 20)->default('')->comment('job');
            $table->string('labels', 255)->default('')->comment('');
            $table->tinyInteger('status')->default(0)->comment('');
            $table->timestamps();
            $table->index(['user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_portfolios');
    }
}
