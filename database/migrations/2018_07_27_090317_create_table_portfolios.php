<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePortfolios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 20)->default('')->comment('项目名');
            $table->string('logo', 255)->default('')->comment('logo');
            $table->string('image', 255)->default('')->comment('展示图片');
            $table->string('link', 255)->default('')->comment('项目链接');
            $table->string('about', 255)->default('')->comment('简短描述');
            $table->text('description')->comment('描述');
            $table->text('list')->comment('栏目');
            $table->boolean('deleted')->default(0)->comment('禁用状态');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('portfolios');
    }
}
