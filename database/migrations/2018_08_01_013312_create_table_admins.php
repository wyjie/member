<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAdmins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 60)->default('')->unique()->index();
            $table->string('password', 60)->default('');
            $table->string('name', 60)->default('');
            $table->rememberToken()->comment('记住密码');
            $table->boolean('deleted')->default(false)->comment('删除标志');
            $table->dateTime('created_at')->default('1970-10-10 00:00:00')->comment('注册时间');
            $table->dateTime('updated_at')->default('1970-10-10 00:00:00')->comment('更新时间');
        });

        DB::statement("ALTER TABLE `admins` comment '管理员表'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admins');
    }
}
