<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserEdus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_edus', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->default(0)->comment('用户id');
            $table->string('edu_name', 50)->default('')->comment('学校名字');
            $table->string('science', 100)->default('')->comment('专业');
            $table->dateTime('start_at')->default('2018-01-01 00:00:00')->comment('start at');
            $table->dateTime('end_at')->default('2018-01-01 00:00:00')->comment('end at');
            $table->timestamps();
            $table->index(['user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_edus');
    }
}
