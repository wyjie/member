<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 16-12-29
 * Time: 上午10:20
 */

namespace App\Services;

use Image;

use Illuminate\Support\Facades\Storage;

class ImageService
{
    const PORTFOLIOS_LOGO = 'PORTFOLIOS_LOGO';
    const PORTFOLIOS_IMAGE = 'PORTFOLIOS_IMAGE';
    const PORTRAIT = 'PORTRAIT';

    protected $basePath;
    protected $baseUrl;

    protected $folders;

    public function __construct()
    {
        $this->basePath = public_path('uploads').'/images/';
        $this->baseUrl = 'uploads/images/';

        $this->folders[self::PORTFOLIOS_LOGO] = 'portfolios_logo/';

        $this->folders[self::PORTFOLIOS_IMAGE] = 'portfolios_image/';
        $this->folders[self::PORTRAIT] = 'portrait/';

        foreach ($this->folders as $folder) {
            $path = $this->basePath.$folder;
            if (!file_exists($path)) {
                @mkdir($path, 0777, true);
            }
        }
    }

    protected function uniqueFilename($content = null)
    {
        if (empty($content)) {
            return str_random(32);
        }
        return md5($content);
    }

    public function put($key, $item, $type, $content = null)
    {
        if ($content == null) {
            $content = $type;
            $type = 0;
        }

        if (!isset($this->folders[$key])) {
            throw new \Error('Invalid upload folder.');
        }

        switch ($key) {
            case ImageService::PORTFOLIOS_LOGO:
                return $this->putPortfoliosLogo($item, $type, $content);
            case ImageService::PORTFOLIOS_IMAGE:
                return $this->putPortfoliosImage($item, $type, $content);
            case ImageService::PORTRAIT:
                return $this->putPortrait($item, $type, $content);
        }

        throw new \Error('Invalid upload target.');
    }

    private function saveToS3($key, $content)
    {
        $image = Image::make($content)->encode('jpg', 75);
        if ($image) {
            $filename = $this->uniqueFilename($content);

            $folder = $this->folders[$key];

            if (env('APP_ENV', 'local') == 'local') {
                $image->save($this->basePath . $folder . $filename . '.jpg');

                return Storage::disk('local')->url($this->baseUrl . $folder . $filename . '.jpg');
            } else {
                Storage::disk('s3')->put($folder . $filename . '.jpg', $image->__toString());
                return Storage::disk('s3')->url($folder . $filename . '.jpg');
            }
        }
        return false;
    }

    protected function putPortfoliosLogo($item, $type, $content)
    {
        $url = $this->saveToS3(ImageService::PORTFOLIOS_IMAGE, $content);
        if ($url) {
            return $url;
        }

        return false;
    }

    protected function putPortfoliosImage($item, $type, $content)
    {
        $url = $this->saveToS3(ImageService::PORTFOLIOS_IMAGE, $content);
        if ($url) {
            return $url;
        }

        return false;
    }

    protected function putPortrait($item, $type, $content)
    {
        $url = $this->saveToS3(ImageService::PORTFOLIOS_IMAGE, $content);
        if ($url) {
            return $url;
        }

        return false;
    }
}

