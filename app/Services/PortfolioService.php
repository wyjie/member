<?php

namespace App\Services;

use App\Models\Portfolio;

class PortfolioService
{
    public function portfolioIndex($count = 20)
    {
        $portfolios = Portfolio::where('deleted', 0)->paginate($count);
        $page = ceil(Portfolio::count());

        return ['list' =>$portfolios, 'page' => $page];
    }

    public function index($count = 20)
    {
        $portfolios = Portfolio::paginate($count);
        $page = ceil(Portfolio::count());

        return ['list' =>$portfolios, 'page' => $page];
    }
}