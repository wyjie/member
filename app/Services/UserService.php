<?php

namespace App\Services;

use App\Models\User;
use App\Models\UserEdu;
use App\Models\UserLabel;
use App\Models\UserPortfolio;
use App\Models\UserRole;

class UserService
{
    public function index($count)
    {
        $users = User::paginate($count);
        $pages = ceil(User::count() / $count);
        return ['users' => $users, 'pages' => $pages];
    }

    public function show($id)
    {
        return User::where('id', $id)->firstOrFail();
    }

    public function update($id, $payload)
    {
        $keys = [
            'password', 'email', 'mobile', 'name', 'last_name', 'key', 'first_name' ,'gender',  'birthday',
            'member_type', 'experience', 'portrait', 'location', 'tools', 'remember_token', 'profile_verified', 'description',
            'deleted', 'amazing', 'preferred_environment', 'experience_body'
        ];

        $data = [];
        foreach ($keys as $key) {
            if (isset($payload[$key])) {
                $data[$key] = $payload[$key];
            }
            if ($key == 'profile_verified') {
                $data[$key] = isset($payload['profile_verified']) && $payload['profile_verified'] == 'on' ? 1 : 0;
            }

            if ($key == 'experience_body') {
                $data[$key] = isset($payload['experience_body']) ? json_encode($payload['experience_body']) : '[]';
            }
        }

        $data['full_name'] = $payload['first_name'] . ' ' . $payload['last_name'];

        User::where('id', $id)
            ->update($data);

        //portfolios
        //删除原有的
        if (isset($payload['portfolios'])) {
            UserPortfolio::where('user_id', $id)->delete();
            $portfolios = $payload['portfolios'];
            foreach ($portfolios as $portfolio) {
                $status = 0;
                if ((isset($portfolio['check']) && $portfolio['check'] == 'on')) {
                    $status = 1;
                }

                if (empty($portfolio['end_at'])) {
                    $portfolio['end_at'] = date('Y-m-d H:i:s');
                }
                if (empty($portfolio['start_at'])) {
                    $portfolio['start_at'] = date('Y-m-d H:i:s');
                }

                if (empty($portfolio['job'])) {
                    $portfolio['job'] = '';
                }

                if (empty($portfolio['tips'])) {
                    $portfolio['tips'] = '';
                }

                $portfolio['end_at'] = date('Y-m-d H:i:s', strtotime($portfolio['end_at']));
                $portfolio['start_at'] = date('Y-m-d H:i:s', strtotime($portfolio['start_at']));

                UserPortfolio::create([
                    'start_at'      => $portfolio['start_at'],
                    'end_at'        => $portfolio['end_at'],
                    'portfolio_id'  => $portfolio['pid'],
                    'user_id'       => $id,
                    'labels'        => $portfolio['labels'],
                    'status'        => $status,
                    'portfolio_name'=> $portfolio['pname'],
                    'job'           => $portfolio['job'],
                    'tips'          => $portfolio['tips']
                ]);
            }
        }


        //skills
        if (isset($payload['skills'])) {
            UserLabel::where('user_id', $id)->delete();
            $skills = $payload['skills'];
            foreach ($skills as $skill) {
                if (!empty($skill['name']) && !empty($skill['time'])) {
                    UserLabel::create([
                        'user_id' => $id,
                        'label_type' => 1,
                        'label_name' => $skill['name'],
                        'time' => $skill['time']
                    ]);
                }
            }
        }

        //edu
        if (isset($payload['education'])) {
            UserEdu::where('user_id', $id)->delete();
            $educations = $payload['education'];
            foreach ($educations as $education) {
                if (!empty($education['university'])) {
                    UserEdu::create([
                        'user_id' => $id,
                        'edu_name' => $education['university'],
                        'science' => $education['discipline'],
                        'start_at' => $education['start_at'],
                        'end_at' => $education['end_at']
                    ]);
                }
            }
        }


    }

    public function store($payload)
    {
        if (isset($payload['key']) || empty($payload['key'])) {
            $payload['key'] = $payload['first_name'];
        }
        $experienceBody = isset($payload['experience_body']) ? json_encode($payload['experience_body']) : '[]';
        $user = User::create([
            'email'         => $payload['email'],
            'mobile'        => isset($payload['mobile']) ? $payload['mobile'] : ' ',
            'last_name'     => $payload['last_name'],
            'first_name'    => $payload['first_name'],
            'key'           => $payload['key'],
            'full_name'     => $payload['first_name'] . ' ' . $payload['last_name'],
            'profile_verified' => isset($payload['profile_verified']) && $payload['profile_verified'] == 'on' ? 1 : 0,
            'experience'    => $payload['experience'],
            'member_type'   => $payload['member_type'],
            'location'      => $payload['location'],
            'portrait'      => $payload['portrait'],
            'description'   => $payload['description'],
            'deleted'       => 0,
            'gender'        => $payload['gender'],
            'tools'         => $payload['tools'],
            'preferred_environment' => $payload['preferred_environment'],
            'amazing'       => $payload['amazing'],
            'experience_body'    => $experienceBody,
        ]);


        //portfolios
        $portfolios = isset($payload['portfolios']) ? $payload['portfolios'] : [];
        foreach ($portfolios as $portfolio) {
            $status = 0;
            if ((isset($portfolio['check']) && $portfolio['check'] == 'on')) {
                $status = 1;
            }

            if (empty($portfolio['end_at'])) {
                $portfolio['end_at'] = date('Y-m-d H:i:s');
            }
            if (empty($portfolio['start_at'])) {
                $portfolio['start_at'] = date('Y-m-d H:i:s');
            }

            if (empty($portfolio['job'])) {
                $portfolio['job'] = '';
            }

            if (empty($portfolio['tips'])) {
                $portfolio['tips'] = '';
            }

            $portfolio['end_at'] = date('Y-m-d H:i:s', strtotime($portfolio['end_at']));
            $portfolio['start_at'] = date('Y-m-d H:i:s', strtotime($portfolio['start_at']));

            UserPortfolio::create([
                'start_at'      => $portfolio['start_at'],
                'end_at'        => $portfolio['end_at'],
                'portfolio_id'  => $portfolio['pid'],
                'user_id'       => $user->id,
                'labels'        => $portfolio['labels'],
                'status'        => $status,
                'portfolio_name'=> $portfolio['pname'],
                'job'           => $portfolio['job'],
                'tips'          => $portfolio['tips']
            ]);
        }

        //skills
        $skills = isset($payload['skills']) ? $payload['skills'] : [];
        foreach ($skills as $skill) {
            if (!empty($skill['name']) && !empty($skill['time'])) {
                UserLabel::create([
                    'user_id' => $user->id,
                    'label_type' => 1,
                    'label_name' => $skill['name'],
                    'time' => $skill['time']
                ]);
            }
        }

        //edu
        $educations = isset($payload['education']) ? $payload['education'] : [];
        foreach ($educations as $education) {
            if (!empty($education['university'])) {
                UserEdu::create([
                    'user_id' => $user->id,
                    'edu_name' => $education['university'],
                    'science' => $education['discipline'],
                    'start_at' => $education['start_at'],
                    'end_at' => $education['end_at']
                ]);
            }
        }
    }


    public function destroy($id, $status)
    {
        $user = User::where('id', $id)->firstOrFail();
        $user->deleted = $status;
        $user->save();
    }

    public function addSkills($id, $skills)
    {
        $user = User::where('id', $id)->first();
        UserLabel::where('user_id', $id)->delete();
        $skills = explode(',', $skills);

        foreach ($skills as $skill) {
            UserLabel::create([
                'user_id' => $id,
                'label_type' => 1,
                'label_name' => trim($skill),
                'time' => (int)$user->experience * 12
            ]);
        }
    }
}