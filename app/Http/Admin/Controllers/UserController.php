<?php

namespace App\Http\Admin\Controllers;

use App\Http\Admin\Requests\UserCreateRequest;
use App\Http\Admin\Requests\UserUpdateRequest;
use App\Models\User;
use App\Models\UserEdu;
use App\Models\UserLabel;
use App\Models\UserPortfolio;
use App\Services\PortfolioService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    private $userService;
    private $portfolioService;

    public function __construct(UserService $userService, PortfolioService $portfolioService)
    {
        $this->middleware('auth');
        $this->userService = $userService;
        $this->portfolioService = $portfolioService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $count = $request->input('count', 20);

        try {
            $data = $this->userService->index($count);
            $users = $data['users'];
            $pages = $data['pages'];

        } catch (\Exception $e) {
            $users = []; //404
            $pages = 0;
            \Log::debug($e);
        }

        return view('admin.user.list', ['users' => $users, 'pages' => $pages]);
    }

    public function create(UserCreateRequest $request)
    {
        $payload = $request->all();

        try {
            $this->userService->store($payload);
        } catch (\Exception $e) {
            \Log::debug($e);
        }

        return response()->redirectTo('/admin/user');

    }

    public function show()
    {
        $portfolios = $this->portfolioService->portfolioIndex(999);

        return view('admin.user.create', ['portfolios' => $portfolios['list']]);
    }

    public function edit($id)
    {
        $user = User::where('id', $id)->first();
        $skills = UserLabel::where('label_type', 1)
            ->where('user_id', $id)
            ->get();
        $portfolios = $this->portfolioService->portfolioIndex(999)['list'];
        $userPortfolios = UserPortfolio::where('user_id', $id)->get();
        $edus = UserEdu::where('user_id', $id)->get();
        $experienceBody = json_decode($user->experience_body);

        foreach ($userPortfolios as $userPortfolio) {
            $userPortfolio->start_at = date('Y-m-d', strtotime($userPortfolio->start_at));
            $userPortfolio->end_at = date('Y-m-d', strtotime($userPortfolio->end_at));
            //$userPortfolio->tips = explode(';', $userPortfolio->tips);
        }

        foreach ($edus as $edu) {
            $edu->start_at = date('Y-m-d', strtotime($edu->start_at));
            $edu->end_at = date('Y-m-d', strtotime($edu->end_at));
        }

        return view('admin.user.edit', ['experienceBody' => $experienceBody, 'userPortfolios' => $userPortfolios, 'user' => $user, 'skills' => $skills, 'portfolios' => $portfolios, 'edus' => $edus]);
    }

    public function update(UserUpdateRequest $request)
    {
        $payload = $request->input();
        $id = $payload['id'];

        try {
            $this->userService->update($id, $payload);
        } catch (\Exception $e) {
            \Log::debug($e);

            return 'update error';
        }

        return response()->redirectTo('/admin/user');
    }


    public function status($id, $status)
    {
        try {
            $this->userService->destroy($id, $status);
        } catch (\Exception $e) {
            \Log::debug($e);

            return response()->json(['code' => 0]);
        }

        return response()->json(['code' => 1]);
    }

    public function skillsShow()
    {
        $users = User::select('id', 'full_name')->get();

        return view('admin.user.skills', ['users' => $users]);
    }

    public function skills(Request $request)
    {
        $skills = $request->input('skills');
        $id = $request->input('id');
        $this->userService->addSkills($id, $skills);

        return response()->redirectTo('/admin/user/'.$id);
    }
}
