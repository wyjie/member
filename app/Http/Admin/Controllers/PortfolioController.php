<?php
namespace App\Http\Admin\Controllers;


use App\Models\Portfolio;
use App\Services\PortfolioService;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    private $portfolioService;

    public function __construct(PortfolioService $portfolioService)
    {
        $this->middleware('auth');
        $this->portfolioService = $portfolioService;
    }

    public function index(Request $request)
    {
        $count = $request->input('count', 20);
        $data = $this->portfolioService->index($count);
        $list = $data['list'];
        $page = $data['page'];

        return view('admin.portfolio.list', ['list' => $list, 'page' => $page]);
    }

    public function show()
    {
        return view('admin.portfolio.create');
    }

    public function create(Request $request)
    {
        $payload = $request->all();
        Portfolio::create([
            'name' => $payload['name'],
            'link' => $payload['link'],
            'about' => $payload['about'],
            'logo' => $payload['logo'],
            'image' => $payload['image'],
            'description' => $payload['description'],
            'list' => json_encode([]),
        ]);

        return response()->redirectTo('/admin/portfolio');
    }

    public function edit($id)
    {
        $portfolio = Portfolio::where('id', $id)->first();

//        if (!empty($portfolio)) {
//            $portfolio->list = json_decode($portfolio->list);
//        }

        return view('admin.portfolio.edit', ['data' => $portfolio]);
    }

    public function update(Request $request)
    {
        $payload = $request->all();
        $id = $payload['id'];

        $keys = [
            'name', 'logo', 'image', 'link', 'about', 'description', 'list'
        ];

        $data = [];
        foreach ($keys as $key) {
            if (isset($payload[$key])) {
                $data[$key] = $payload[$key];

                if ($key == 'list') {
                    $data[$key] = json_encode($payload[$key]);
                }
            }
        }


        Portfolio::where('id', $id)
            ->update($data);

        return response()->redirectTo('/admin/portfolio');
    }

    public function status($id, $status)
    {
        try {
            $portfolio = Portfolio::where('id', $id)->firstOrFail();
            $portfolio->deleted = $status;
            $portfolio->save();
        } catch (\Exception $e) {
            \Log::debug($e);

            return response()->json(['code' => 0]);
        }

        return response()->json(['code' => 1]);
    }
}
