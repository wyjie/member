<?php
namespace App\Http\Admin\Controllers;

use App\Services\ImageService;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    private $imageService;

    public function __construct(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    public function putLogo(Request $request)
    {
        $image = $request->file('file');
        $image = file_get_contents($image->getRealPath());

        try {
            $url = url(substr($this->imageService->put(ImageService::PORTFOLIOS_LOGO, null, $image), 9));
        } catch (\Exception $e) {
            \Log::debug($e);
            $url = false;
        }

        return response()->json(['url' => $url]);
    }

    public function putImage(Request $request)
    {
        $image = $request->file('file');
        $image = file_get_contents($image->getRealPath());

        try {
            $url = url(substr($this->imageService->put(ImageService::PORTFOLIOS_IMAGE, null, $image), 9));
        } catch (\Exception $e) {
            \Log::debug($e);
            $url = false;
        }

        return response()->json(['url' => $url]);
    }

    public function putPortrait(Request $request)
    {
        $image = $request->file('file');
        $image = file_get_contents($image->getRealPath());

        try {
            $url = url(substr($this->imageService->put(ImageService::PORTRAIT, null, $image), 9));
        } catch (\Exception $e) {
            \Log::debug($e);
            $url = false;
        }

        return response()->json(['url' => $url]);
    }
}
