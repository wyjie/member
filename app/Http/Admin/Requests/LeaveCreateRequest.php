<?php

namespace App\Http\Requests;

class LeaveCreateRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'tcid' => [
                'required'
            ],
            'length' => [
                'required'
            ],
            'type' => [
                'required',
                'in:4,5,6,7,8,9,10,11'
            ],
            'start_at' => [
                'required'
            ],
            'end_at' => [
                'required'
            ]
        ];
        return $rules;
    }
}