<?php
namespace App\Http\Admin\Requests;

class UserCreateRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'first_name' => [
                'required',
            ],
            'email' => [
                'required',
                'email',
                'unique:users,email'
            ],

        ];
        return $rules;
    }
}