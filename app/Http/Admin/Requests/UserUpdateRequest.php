<?php
namespace App\Http\Admin\Requests;

class UserUpdateRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $payload = $this->validationData();
        $rules = [
            'first_name' => [
                'required',
            ],
            'email' => [
                'required',
                'email',
                'unique:users,email,'.$payload['id']
            ],
        ];
        return $rules;
    }
}