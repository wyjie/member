<?php

namespace App\Http\Admin\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Http\JsonResponse;

class BaseRequest extends FormRequest
{
    public function response(array $errors)
    {
        foreach ($errors as $k => $val){
            $errors[$k] = $val[0];
        }

        return new JsonResponse($errors, 422);
    }
}
