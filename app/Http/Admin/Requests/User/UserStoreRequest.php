<?php
namespace App\Http\Requests\User;

use App\Http\Requests\BaseRequest;

class UserStoreRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'first_name' => [
                'required',
            ],
            'email' => [
                'required',
                'email',
                'unique:users,email'
            ],

        ];
        return $rules;
    }
}