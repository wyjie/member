<?php
namespace App\Http\Web\Controllers;

use App\Models\User;
use App\Models\UserEdu;
use App\Models\UserLabel;
use App\Models\UserPortfolio;

class UserController extends  Controller
{
    public function index($key = 'aaron')
    {
        $user = User::where('key', $key)->where('deleted', 0)->first();
        if (empty($user)) {
            return '用户不存在';
        }
        $id = $user->id;
        $user->tools = explode(',', $user->tools);

        $skills = UserLabel::where('label_type', 1)
            ->where('user_id', $id)
            ->get();
        $portfolios = UserPortfolio::join('portfolios', 'portfolios.id', '=', 'user_portfolios.portfolio_id')
            ->select('portfolios.logo as logo')
            ->addSelect('portfolios.image as image')
            ->addSelect('portfolios.description as description')
            ->addSelect('portfolios.about as about')
            ->addSelect('user_portfolios.tips as list')
            ->addSelect('portfolios.name as name')
            ->addSelect('portfolios.link as link')
            ->addSelect('user_portfolios.job as job')
            ->addSelect('user_portfolios.start_at as start_at')
            ->addSelect('user_portfolios.end_at as end_at')
            ->addSelect('user_portfolios.labels as labels')
            ->where('status', 1)
            ->where('user_id', $id)
            ->get();
        $edus = UserEdu::where('user_id', $id)->get();

        $portfolios->transform(function ($item) {
            $item->list = explode(',', $item->list);
            $item->labels = explode(',', $item->labels);

            return $item;
        });

        $experienceBody = json_decode($user->experience_body);


        $data = [
            'time' => date('F', strtotime('+1 month')),
            'user' => $user,
            'edus' => $edus,
            'skills' => $skills,
            'portfolios' => $portfolios,
            'experience_body' => $experienceBody
        ];

        return view('web.user.member2', $data);
    }

}