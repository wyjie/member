<?php
namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class UserEdu extends Authenticatable
{
    protected $fillable = [
        'user_id', 'edu_name', 'science', 'start_at', 'end_at'
    ];
}