<?php
namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $fillable = [
        'password', 'email', 'mobile', 'name', 'last_name', 'first_name', 'key', 'full_name' ,'gender',  'birthday',
        'member_type', 'experience', 'experience_body', 'portrait', 'location', 'preferred_environment', 'amazing', 'tools', 'remember_token', 'profile_verified', 'description',
        'deleted'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'created_at', 'updated_at',  'remember_token'
    ];
}