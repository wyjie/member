<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;

class UserPortfolio extends Model
{
    protected $fillable = [
        'user_id', 'portfolio_id', 'portfolio_name', 'job', 'start_at', 'end_at', 'labels', 'tips', 'status'
    ];
}