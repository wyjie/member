<?php
namespace App\Models;

use \Illuminate\Database\Eloquent\Model;

class UserLabel extends Model
{
    protected $fillable = [
       'user_id', 'label_name', 'label_type', 'time'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}